import itertools
from deap import algorithms, base, creator, tools
import statistics

from math import floor, ceil

from more_itertools import take
from nltk import word_tokenize, OrderedDict
import re, string
import random

# class constants
num_words = 10  # number of words to print
n_gen = 250  # Increase to possibly improve best individual
pop_size = 500
prob_xover = 0.8
prob_mut = 0.2
tournament_size = 10
num_iterations = 16
dict_freq = OrderedDict()  # frequency map for words in list
dict_paired_order = OrderedDict()
dict_paired_freq = OrderedDict()


f = open('input.txt', 'r', encoding='utf-8')
words = ''
for line in f:
    words += line

regex = re.compile('[,.;)(\"\'!?_-]')
words = regex.sub('', words)
tokenized_words = word_tokenize(words) # list of all words in new dictionary
WORD_LIST = list(set(tokenized_words)) # create a set out of words so there are no repeats in word list and make list again
dictionary_size = len(WORD_LIST)


# create a frequency dictionary of all the words in the text
for word in tokenized_words:
    if word not in dict_freq:
        dict_freq[word] = 1
    else:
        dict_freq[word] += 1

# create a frequency map of paired words in the text
for i in range(len(tokenized_words) - 1):
    if (tokenized_words[i+1],tokenized_words[i]) not in dict_paired_freq:
        dict_paired_freq[(tokenized_words[i+1],tokenized_words[i])] = 1
    else:
        dict_paired_freq[(tokenized_words[i+1],tokenized_words[i])] += 1

dict_paired_order = dict(zip(dict_paired_freq.keys(), list(range(len(dict_paired_freq.keys())))))

               #NOME DO OBJETO   #TIPO         #PARAMETROS DO OBJETO
creator.create("FitnessMin",     base.Fitness, weights=(1.0,))
creator.create("Individual",     list,         fitness=creator.FitnessMin)



def getFitness(individual):
    ind_sentence = ''
    fitness = 0
    # numbers = [dict_freq[key] for key in dict_freq]
    # mean = statistics.mean(numbers)

    # Essa parte do codigo penaliza palavras que aparecem muitas vezes durante o texto
    # i.e: 15 vezes/ mean vezes
    for word_index in individual:
        ind_sentence += tokenized_words[word_index] + ' '
        if dict_freq[tokenized_words[word_index]] > 10:
            fitness -= 2

    tokenized_sentence = word_tokenize(ind_sentence)
    for i in range(len(tokenized_sentence) - 1):
        if (tokenized_sentence[i+1], tokenized_sentence[i]) in dict_paired_freq:

            original_pair_order = dict_paired_order.get((tokenized_sentence[i+1], tokenized_sentence[i]))

            OldRange = (len(dict_paired_order))
            NewRange = (len(tokenized_sentence))

            relative_text_order = (((original_pair_order) * NewRange) / OldRange)

            fitness += dict_paired_freq[(tokenized_sentence[i+1], tokenized_sentence[i])]

            # BALAQUINHA QUE FIZEMOS PARA LEVAR EM CONTA A ORDEM DAS PALAVRAS
            # fitness += -ceil(abs(i-relative_text_order))

    return fitness,


toolbox = base.Toolbox()
# randomly placed words in a list ranging from 0-dictionary_size
toolbox.register("indices", random.sample, range(dictionary_size), num_words)
# gerando o objeto individuo no toolbox, do tipo ceator.Individual
toolbox.register("individual", tools.initIterate, creator.Individual, toolbox.indices)
# população sera uma lista do tipo individuo
toolbox.register("population", tools.initRepeat, list, toolbox.individual)

# funcao de fitness
toolbox.register("evaluate", getFitness)
# cross Over, podemos mudar aqui, ver os métodos iniciados com cx
toolbox.register("mate", tools.cxUniform, indpb=0.05)
# criterio de mutação, ver metodos iniciados com mut
toolbox.register("mutate", tools.mutFlipBit, indpb=0.05)
# criterio de selecao, ver metodos iniciados com sel
toolbox.register("select", tools.selRoulette)

top_sentences = []
top_sentences_words = []

# Repeat algorithm fithteen times over 150 generations
for num_sentence in range(num_iterations):
    pop = toolbox.population(n=pop_size)
    bestfitlist = []
    bestfitval = 0;
    # Let's collect the fitness values from the simulation using
    fitnesses = list(map(toolbox.evaluate, pop))
    for ind, fit in zip(pop, fitnesses):
        ind.fitness.values = fit

    #agora que os individuos ja possuem fitness, podemos rodar o processo de selecao
    for g in range(0, n_gen):
        # Start creating the children (or offspring)

        # First, Apply selection:
        offspring = toolbox.select(pop, pop_size) # ele pega o fitness de dentro do objeto e recalcula
        # Clone the selected individuals
        offspring = list(map(toolbox.clone, offspring))

        # Apply variations (xover and mutation), Ex: algorithms.varAnd(?, ?, ?, ?)
        offspring = algorithms.varAnd(offspring, toolbox, prob_xover, prob_mut)

        for fit in fitnesses:
            if bestfitval < fit[0]:
                bestfitval = fit[0]
        bestfitlist.append(bestfitval)

    # Let's evaluate the fitness of each individual.
    fitnesses = list(map(toolbox.evaluate, offspring))

    for ind, fit in zip(offspring, fitnesses):
        ind.fitness.values = fit
    # One way of implementing elitism is to combine parents and children to give them equal chance to compete:
    # For example: pop[:] = pop + offspring
    # Otherwise you can select the parents of the generation from the offspring population only: pop[:] = offspring
    pop[:] = pop + offspring #elitismo
    # pop[:] = offspring
    print("-- End of evolution --  Number of generations: " + str(n_gen))
    print("Best fitness values list: %s" % (bestfitlist))

    # seleciona a melhor resposta
    best_ind = tools.selBest(pop, 1)[0]
    best_ind_sentence = ''
    # itera sobre as palavras tokenizadas pra gerar a frase
    for i in best_ind:
        if i > dictionary_size:
            i = dictionary_size - 1
        best_ind_sentence += tokenized_words[i] + ' '

    display_sentence = "Sentence %s: \"%s\", best fitness: %s" % (num_sentence + 1, best_ind_sentence, bestfitlist[len(bestfitlist) - 1])
    print(display_sentence)
    top_sentences.append(display_sentence)
    top_sentences_words.append(best_ind_sentence.split())


most_freq_words_dict = OrderedDict()

print("-- End of the %d evolutions --\n" % num_iterations)
best_ind = tools.selBest(pop, 1)[0]
print("The Top %d Sentences Generated:" % num_words)
for sentence in top_sentences:
    print(sentence)

for sentence in top_sentences_words:
    for word in sentence:
        if word not in most_freq_words_dict:
            most_freq_words_dict[word] = 1
        else:
            most_freq_words_dict[word] += 1

most_freq_words_dict = OrderedDict(sorted(most_freq_words_dict.items(), key=lambda x : x[1], reverse=True))

print(take(5, most_freq_words_dict.items()))



